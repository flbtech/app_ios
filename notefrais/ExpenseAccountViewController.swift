//
//  ExpenseAccountViewController.swift
//  notefrais
//
//  Created by stagiaire on 20/03/2017.
//  Copyright © 2017 erpssi. All rights reserved.
//

import UIKit
import ImagePicker
import Lightbox
import Alamofire
import Validator
import Foundation

class ExpenseAccountViewController: UIViewController, ImagePickerDelegate {
    
    
    var tokenConnection:String? = ""
    var idEmployee:String? = ""
    
    @IBOutlet weak var labelTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var totalTextField: UITextField!
    @IBOutlet weak var tvaSlider: UISlider!
    @IBOutlet weak var tvaValueLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var ImageViewPicker: UIImageView!
    @IBOutlet weak var imageViewButtonPicker: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get the token from userDefaults
        let db = UserDefaults.standard
        self.tokenConnection = db.string(forKey: "token")
        self.idEmployee = db.string(forKey: "idEmployee")
        if self.tokenConnection == nil {
            //            let connectionViewController = storyboard!.instantiateViewController(withIdentifier: "ConnexionViewController")
            //            present(connectionViewController, animated: true, completion: nil)
            print("here we need to present back the login view")
        }
        //
        
        //Let show numberPad for the totalTextField
        self.totalTextField.keyboardType = UIKeyboardType.decimalPad
        
        //Link the button to ImagePickerController
        self.imageViewButtonPicker.addTarget(self, action: #selector(buttonTouched(imageViewButtonPicker:)), for: .touchUpInside)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Methods to handle the behaviour of the ImagePickerController
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        
        let lightboxImages = images.map {
            return LightboxImage(image: $0)
        }
        
        let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        imagePicker.present(lightbox, animated: true, completion: nil)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.ImageViewPicker.image = images.first
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    // Fields Handler (Slide, Date, ImageView with ImagePicker)
    
    /* Launch ImagePickerController */
    func buttonTouched(imageViewButtonPicker: UIButton) {
        let imagePicker = ImagePickerController()
        imagePicker.imageLimit = 1
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    /* Change the value of the tvaValueLabel */
    @IBAction func onSliderValueChange(_ sender: UISlider) {
        let value = roundf(sender.value / 0.1) * 0.1
        self.tvaValueLabel.text = "\(value)"
    }
    
    //Validate and submit Form
    @IBAction func validateAndSubmitAction(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        //Get the input value of the form
        let label = self.labelTextField.text
        let description = self.descriptionTextField.text
        let total = self.totalTextField.text
        let tva = self.tvaValueLabel.text
        let date = dateFormatter.string(from: self.datePicker.date)
        let image = self.ImageViewPicker.image

        
        //Validator
        let minLengthRule = ValidationRuleLength(min: 5, error: ValidationError(message: "Cinq caractères minimum ! (Intitulé & description)"))
        let minLengthRuleTotal = ValidationRuleLength(min: 1, error: ValidationError(message: "Montant requis !"))
        let resultLabel = label!.validate(rule: minLengthRule)
        let resultDescription = description!.validate(rule: minLengthRule)
        let resultTotal = total!.validate(rule: minLengthRuleTotal)
        let result = ValidationResult.merge(results: [resultLabel, resultDescription, resultTotal])
        
        switch result {
        case .invalid(let failures):
            let validationError = failures.first as! ValidationError
            let prompt = UIAlertController(title: "Saisie incorrecte", message: validationError.message , preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Ok", style: .default)
            prompt.addAction(cancel)
            present(prompt, animated: true)
            return
        default:
            break
        }
    
        
        //set the token for the api in the header
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + self.tokenConnection!
        ]
        
        //construct the parameters to be sent
        let parameter: [String: AnyObject] = [
            "label": label as AnyObject,
            "description": description as AnyObject,
            "total": total as AnyObject,
            "tva": tva as AnyObject,
            "date_expense_account": date as AnyObject
        ]
        
        let datas: [String: Dictionary] = [
            "data": parameter
        ]
        
        //Two requests to create a expense account
        //First one, we create the expense account with the text datas
        Alamofire.request(AppDelegate.URL_SERVER + "expense-accounts/\(self.idEmployee!)", method: .post , parameters: datas , encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if(response.result.isSuccess) {
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    let data = JSON.value(forKey: "data") as! NSDictionary
                    let idEmployee = data.value(forKey: "id")
                    let url = "expense-accounts/\(idEmployee!)/image"
                    
                    //Secondly, we link the image of the form to the expense account previously created
                    Alamofire.upload(multipartFormData: { (multipartFormData) in
                        multipartFormData.append(UIImageJPEGRepresentation(image!, 1)!, withName: "image", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                    }, to:AppDelegate.URL_SERVER + url, method: .post, headers: headers)
                    { (result) in
                        switch result {
                        case .success(let upload, _, _):
                            
                            upload.uploadProgress(closure: { (progress) in
                            })
                            
                            upload.responseJSON { response in
                                self.dismiss(animated: true, completion: nil)
                            }
                            
                            
                        case .failure(let encodingError):
                            print (encodingError)
                        }
                    }
                }
            } else if (response.result.isFailure){
                
            }
        }
    }
    
    @IBAction func cancelAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
