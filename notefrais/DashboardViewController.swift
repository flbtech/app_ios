//
//  DashboardViewController.swift
//  notefrais
//
//  Created by stagiaire on 22/03/2017.
//  Copyright © 2017 erpssi. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration
import Foundation

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var logoutItem: UIToolbar!
    @IBOutlet weak var listItem: UIBarButtonItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var buttonImage: UIButton!
    @IBOutlet weak var imageViewStack: UIImageView!
    @IBOutlet weak var buttonImageStack: UIButton!
    @IBOutlet weak var nbTotalLabel: UILabel!
    @IBOutlet weak var nbValidatedLabel: UILabel!
    @IBOutlet weak var nbPendingLabel: UILabel!
    @IBOutlet weak var nbDeniedLabel: UILabel!
    @IBOutlet weak var acceptedButton: UIButton!
    @IBOutlet weak var pendingButton: UIButton!
    @IBOutlet weak var deniedButton: UIButton!
    
    let db = UserDefaults.standard
    var dataTableArray:NSArray = []
    var dataTableArrayPending = [NSDictionary]()
    var dataTableArrayAccepted = [NSDictionary]()
    var dataTableArrayDenied = [NSDictionary]()
    var constSegueArray:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.imageView.image = UIImage(named: "add")
        self.imageViewStack.image = UIImage(named: "list")
    }
    
    override func viewWillAppear(_ animated: Bool) {

        if(isConnectedToNetwork()){
            if db.string(forKey: "token") == nil {
                let modal = storyboard!.instantiateViewController(withIdentifier: "ConnexionViewController")
                self.present(modal, animated: true, completion: nil)
            } else {
                self.loadData()
            }
        } else {
            print("no internet")
            let prompt = UIAlertController(title: "Erreur", message: "Cannot reach internet.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { _ in
                exit(0)
            })
            prompt.addAction(okAction)
            self.present(prompt, animated:true, completion: nil)
            
        }
        
         super.viewWillAppear(animated)
    }
    
    func loadData(){
        let accessToken:String? = self.db.string(forKey: "token")
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(accessToken!)",
            "Accept": "application/json",
            "Access-Control-Allow-Origin":"*"
        ]
        
        Alamofire.request(AppDelegate.URL_SERVER+"expense-accounts", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                
                //to get status code
                if let status = response.response?.statusCode {
                    
                    if status == 200 {
                        
                        if let result = response.result.value {
                            let reponseResult:NSDictionary? = result as? NSDictionary
                            self.dataTableArray = reponseResult!.value(forKey: "data") as! NSArray
                            
                            self.traitementData()
                        }
                    }
                }
        }
    }
    
    func traitementData(){
        var countPending:Int = 0
        var countDenied:Int = 0
        var countAccepted:Int = 0
        self.dataTableArrayPending = [NSDictionary]()
        self.dataTableArrayAccepted = [NSDictionary]()
        self.dataTableArrayDenied = [NSDictionary]()
        
        for value in self.dataTableArray {
            let valueData:NSDictionary? = value as? NSDictionary
            let state:Int = valueData?.value(forKey: "state") as! Int
            
            switch state {
                case 1:
                    countPending += 1
                    self.dataTableArrayPending.append(valueData!)
                    break
                case 2:
                    countAccepted += 1
                    self.dataTableArrayAccepted.append(valueData!)
                    break
                case 3:
                    countDenied += 1
                    self.dataTableArrayDenied.append(valueData!)
                    break
                default:
                    break
            }
        }
        
        self.nbTotalLabel.text = String(self.dataTableArray.count)
        self.nbPendingLabel.text = String(countPending)
        self.nbDeniedLabel.text = String(countDenied)
        self.nbValidatedLabel.text = String(countAccepted)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMaster" {
            let controller = segue.destination as! MasterViewController
            
            switch self.constSegueArray {
                case 1:
                    controller.dataTableArray = self.dataTableArrayPending as NSArray
                    break
                case 2:
                    controller.dataTableArray = self.dataTableArrayAccepted as NSArray
                    break
                case 3:
                    controller.dataTableArray = self.dataTableArrayDenied as NSArray
                    break
                default:
                    controller.dataTableArray = self.dataTableArray
                    break
            }
        }
    }

    
    @IBAction func logoutActionItem(_ sender: Any) {
        self.db.removeObject(forKey: "token")
        self.db.removeObject(forKey: "idEmployee")
        
        let modal = storyboard!.instantiateViewController(withIdentifier: "ConnexionViewController")
        self.present(modal, animated: true, completion: nil)
    }
    
    @IBAction func addActionButton(_ sender: Any) {
        let modal = storyboard!.instantiateViewController(withIdentifier: "ea_form")
        self.present(modal, animated: true, completion: nil)
    }
    
    @IBAction func acceptedActionButton(_ sender: Any) {
        self.constSegueArray = 2
        performSegue(withIdentifier: "showMaster", sender: nil)
    }
    
    @IBAction func pendingActionButton(_ sender: Any) {
        self.constSegueArray = 1
        performSegue(withIdentifier: "showMaster", sender: nil)
    }
    
    @IBAction func deniedActionButton(_ sender: Any) {
        self.constSegueArray = 3
        performSegue(withIdentifier: "showMaster", sender: nil)
    }
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired
        
        return isReachable && !needsConnection
        
    }
    
    @IBAction func stackActionButton(_ sender: Any) {
        self.constSegueArray = 0
        performSegue(withIdentifier: "showMaster", sender: nil)
    }
    
    @IBAction func listActionButton(_ sender: Any) {
        self.constSegueArray = 0
        performSegue(withIdentifier: "showMaster", sender: nil)
    }
    
}
