//
//  AppViewController.swift
//  notefrais
//
//  Created by stagiaire on 20/03/2017.
//  Copyright © 2017 erpssi. All rights reserved.
//

import UIKit
import CoreData

class AppViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var managedObjectContext: NSManagedObjectContext? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.imageView.image = UIImage(named: "logo");
        Timer.scheduledTimer(timeInterval: 3,
                                         target: self,
                                         selector: #selector(self.changeView),
                                         userInfo: nil,
                                         repeats: false)
    }
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func changeView(){
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ConnexionViewController") as! ConnexionViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
