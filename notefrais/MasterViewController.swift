
//
//  MasterViewController.swift
//  notefrais
//
//  Created by stagiaire on 20/03/2017.
//  Copyright © 2017 erpssi. All rights reserved.
//

import UIKit
import CoreData
import PageMenu
import Alamofire

class MasterViewController: UITableViewController, NSFetchedResultsControllerDelegate, UISearchBarDelegate {

    var detailViewController: DetailViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil
    let db = UserDefaults.standard
    var dataTableArray:NSArray = []
    
    // search bar
    var searchActive : Bool = false
    var filtered:[Any] = []
    @IBOutlet weak var searchBar: UISearchBar!
    var dataTableDeepClone:NSArray = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        self.dataTableDeepClone = self.dataTableArray.copy() as! NSArray
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        self.reloadData()
        super.viewWillAppear(animated)
    }
    
    
    // --------- SEARCH BAR ---------------------------------
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
       
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let resultPredicate = NSPredicate(format: "description contains[c] %@", searchText)

        filtered = self.dataTableArray.filtered(using: resultPredicate)

        if(filtered.count == 0){
            searchActive = false;
            self.dataTableArray = self.dataTableDeepClone // we need to affect back initial data to cancel search
            
        } else {
            searchActive = true;
            self.dataTableArray = filtered as NSArray
        }
        
        
        self.tableView.reloadData()
    }

    
    func reloadData(){
        // Do any additional setup after loading the view, typically from a nib.
        //self.navigationItem.leftBarButtonItem = self.editButtonItem
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        self.navigationItem.backBarButtonItem = backItem
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func insertNewObject(_ sender: Any) {
        let modal = storyboard!.instantiateViewController(withIdentifier: "ea_form")
        self.present(modal, animated: true, completion: nil)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                //let object = self.fetchedResultsController.object(at: indexPath)
                let object:NSDictionary = self.dataTableArray[indexPath.row] as! NSDictionary
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        }
        return self.dataTableArray.count;
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let data:NSDictionary = self.dataTableArray[indexPath.row] as! NSDictionary
        let description:String = data.value(forKey: "description") as! String
        cell.textLabel!.text = description
        
        // search bar
        if(searchActive){
            let searchResult = filtered[indexPath.row] as! NSDictionary
            cell.textLabel?.text = searchResult.value(forKey: "description") as? String
        } else {
            cell.textLabel?.text = description
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let data:NSDictionary = self.dataTableArray[indexPath.row] as! NSDictionary
            let id:Int = data.value(forKey: "id") as! Int
            let accessToken:String? = self.db.string(forKey: "token")
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken!)",
                "Accept": "application/json",
                "Access-Control-Allow-Origin":"*"
            ]
            
        
    
            Alamofire.request(AppDelegate.URL_SERVER+"expense-accounts/\(id)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { response in
                    
                    //to get status code
                    if let status = response.response?.statusCode {
                        
                        if status == 200 {
                            // Subtibility : /!\ NSArray Type is not MUTABLE !
                            let dataTableArrayDeepClone = self.dataTableArray.mutableCopy() as! NSMutableArray // create a mutable deep clone of the data
                            dataTableArrayDeepClone.removeObject(at: indexPath.row) // remove the item at the right row
                            self.dataTableArray = dataTableArrayDeepClone as NSArray // now, we can affect our datasource
                            self.tableView.reloadData() // tableView is now knowing that some data have been changed, so it will reload the new data
                        }
                    }
            }

        }
    }

    func configureCell(_ cell: UITableViewCell, withEvent event: Event) {
        cell.textLabel!.text = event.timestamp!.description
    }

}

