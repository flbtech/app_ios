//
//  ConnexionViewController.swift
//  notefrais
//
//  Created by stagiaire on 20/03/2017.
//  Copyright © 2017 erpssi. All rights reserved.
//

import UIKit
import Alamofire

class ConnexionViewController: UIViewController {

    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var connexionButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    let db = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.imageView.image = UIImage(named: "dashboard-background");
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated:true);
        super.viewWillAppear(animated)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func connextionActionButton(_ sender: UIButton) {
        let login = self.usernameText.text
        let password = self.passwordText.text
        let parameter: [String: AnyObject] = [
            "_username":login as AnyObject,
            "_password":password as AnyObject
        ]
        
        Alamofire.request(AppDelegate.URL_SERVER+"login_check", method: .post, parameters: parameter, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                //to get status code
                if let status = response.response?.statusCode {
                    
                    if status == 200 {
                        
                        if let result = response.result.value {
                            // token
                            let reponseResult:NSDictionary? = result as? NSDictionary
                            self.db.set(reponseResult!.value(forKey: "token"), forKey: "token")
                            
                            // id employee
                            let dataResponse:NSDictionary? = reponseResult!.value(forKey: "data") as? NSDictionary
                            let employeeResponse:NSDictionary? = dataResponse!.value(forKey: "employee") as? NSDictionary
                            self.db.set(employeeResponse!.value(forKey: "id"), forKey: "idEmployee")
                            
                            // pop view
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
        }
        
        // l
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
